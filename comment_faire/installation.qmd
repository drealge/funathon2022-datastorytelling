---
title: "Installation"
format:
  html:
    toc: true
    toc-location: right
    toc-title: "Sommaire"
    code-fold: true
    code-summary: "Voir le code"
    code-copy: true
---

## Initialisation

### Prérequis

-   Disposer d'un compte Datalab (https://datalab.sspcloud.fr) dont gitlab (https://git.lab.sspcloud.fr)

-   Connaitre son token Gitlab (à générer dans Gitlab --\> User Preferences --\> Access Tokens)

### Créer un projet gitlab avec le code-source fourni

-   Via un fork du projet quarto_dataviz

    -   Aller sur le projet https://git.lab.sspcloud.fr/adelorme/quarto_dataviz

    -   Cliquer sur Fork :![](/images/fork_gitlab.png)

    -   Changer le nom du projet, sa description...

    -   Cliquer sur Fork Project

-   En partant du code source :

    -   Aller sur le projet https://git.lab.sspcloud.fr/adelorme/quarto_dataviz

    -   Cliquer sur Download :

![](/images/source_code.png)

-   Choisir zip

-   Créer un nouveau projet "blank project"

-   Ajouter les fichiers du zip

### Cloner le projet Gitlab

Les sources du projet sont dans Gitlab. Mais elles seront modifiées par un IDE connecté à Gitlab (Rstudio, VScode...). Pour faire le lien entre Gitlabe et l'IDE, il faut cloner le projet (à savoir copier l'url du dépôt Gitlab) :

![](/images/clone_gitlab.png)

### Créer un service Rstudio sur Datalab

-   Se connecter au Datalab

-   Cliquer sur Services Catalog

-   Choisir RStudio --\> Launch

-   Cliquer sur RStudio Configuration

-   Aller sur l'onglet Git

    -   Saisir son identifiant Gitlab

    -   Augmenter le cache (en s) pour éviter de se reconnecter durant le session de travail

    -   Saisir son token Gitlab

    -   Coller l'adresse du projet gitlab cloné précédemment

![](/images/rstudio_service.png)

## Lancer l'installation des packages nécessaires

-   Ouvrir le service Rstudio

-   Cliquer sur le fichier .Rproj dans le répertoire récupéré (ça permet de se mettre dans le projet)

-   Ouvrir et lancer le programme install_packages.R

    -   Renseigner yes dans l'invite de commande (pour l'installation de miniconda)

### Vérifier le bon fonctionnement via Render et un push GitLab

-   Dans le menu de Rstudio : cliquer sur Build --\> Render Website (ou CTRL + SHIFT + B)

-   Dans la fenètre Viewer, vérifier que le site est fonctionnel (possibilité de l'ouvrir en grand avec le bouton "Show in a new window")

![](/images/viewer.png)

-   Faites un Git --\> Push

    -   si le service est mal configuré, il peut demander l'identifiant et le mot de passe --\> mettre le Token

    -   le résultat attendu est :

![](/images/git_push_ok.png)

## Créer des branches

Pour travailler proprement avec Gitlab, il convient de créer des branches pour ne jamais travailler sur le main :

-   dans Rstudio cliquer sur 'New branch'

![](/images/new_branch.png)

-   Puis saisir le nom de la branche

![](/images/name_new_branch.png)
